# Renovate bug - minimum repository reproduction

1. Run renovate
2. Manually update labels on dependency dashboard
3. Run renovate again
4. Notice the labels have reverted on the dependency dashboard